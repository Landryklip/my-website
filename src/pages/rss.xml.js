import rss from "@astrojs/rss";

export const get = () =>
  rss({
    title: "Landry Kalipe | Blog",
    description: "Landry Kalipe's Blog",
    site: "https://landrykalipe.com",
    items: import.meta.glob("./**/*.md"),
    customData: `<language>en-us</language>`,
  });
