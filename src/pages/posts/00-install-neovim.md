---
layout: ../../layouts/MarkdownPostLayout.astro
title: Neovim Different Installation Methods Pros & Cons
pubDate: 2023-08-14
tags: ["neovim"]
---

Je vais vous montrer 3 façons différentes d'installer Neovim. La plupart d'entre vous utilisent probablement le gestionnaire de paquets dont vous disposez, mais il y a aussi de bonnes raisons d'utiliser les autres méthodes.

## Méthode #1 : Gestionnaire de paquets

C'est probablement la façon dont la plupart des gens vont installer Neovim. Les gestionnaires de paquets s'occupent du choix de la version pour vous, de la mise à jour, de la mise à niveau, etc. Ils font abstraction de tous les détails et seront la façon la plus simple d'installer la plupart des choses.

Quelques bonnes raisons de ne pas utiliser votre gestionnaire de paquets sont : ne pas vouloir mettre à jour/mettre à niveau tout en mettant à jour le reste de vos paquets. Ou inversement, vous pouvez vouloir utiliser la dernière version (nightly) pour essayer de nouvelles fonctionnalités.


**Installer**:

  - Arch Linux:
    - `sudo pacman -S neovim`

  - Debian(Ubuntu):
    - `sudo apt install neovim`


**Désinstaller**:

  - Arch Linux:
    - `sudo pacman -R neovim`

  - Debian(Ubuntu):
    - `sudo apt remove neovim`


## Méthode #2 : AppImage


**Installer**:

  ```
  curl -LO https://github.com/neovim/neovim/releases/latest/download/nvim.appimage

  mv nvim.appimage nvim

  chmod u+x nvim

  sudo mv nvim /usr/local/bin

  nvim
  ```

**Désinstaller**:

  ```
  sudo rm /usr/local/bin/nvim
  ```


## Méthode #3 : À partir des sources

Cette méthode est la plus compliquée, mais elle fonctionnera sur toutes les plateformes et vous donnera le plus de contrôle. Elle vous permettra d'extraire n'importe quelle branche et de l'installer. Vous avez même la possibilité de modifier une partie du code si vous le souhaitez.

**Installer**:

  ```
  git clone https://github.com/neovim/neovim.git

  cd neovim

  make CMAKE_BUILD_TYPE=Release

  sudo make install
  ```

Par défaut, la dernière version nocturne sera installée. Si vous souhaitez installer une version différente, vous devrez la vérifier avant de la compiler et de l'installer.

**Exemple:**:

  ```
  git clone https://github.com/neovim/neovim.git

  cd neovim

  git checkout release-0.7 
  # git checkout v0.7.2 # by tag

  make CMAKE_BUILD_TYPE=Release

  sudo make install
  ```

**Désinstaller**:

  ```
  sudo rm /usr/local/bin/nvim
  sudo rm -r /usr/local/share/nvim/
  ```

## Resources

- [r/Neovim install wiki](https://github.com/neovim/neovim/wiki/Installing-Neovim)
