---
layout: ../../layouts/MarkdownPostLayout.astro
title: Installing DWM
pubDate: 2023-09-23
tags: ["dwm"]
---

Assurez-vous d'avoir déjà un utilisateur

## Install Xorg

```
pacman -S xorg-server xorg-xinit xorg-xrandr xorg-xsetroot
```

## Installer git

```
pacman -S git
```

## Installer un navigateur

```
pacman -S firefox
```

## Créer un répertoire de configuration

```
mkdir ~/.config
```

## Installer DWM

```
git clone git://git.suckless.org/dwm ~/.config/dwm
git clone git://git.suckless.org/st ~/.config/st
git clone git://git.suckless.org/dmenu ~/.config/dmenu
```

```
cd ~/.config/dwm && sudo make install
cd ~/.config/st && sudo make install
cd ~/.config/dmenu && sudo make install
```

## Installation d'un Display (DM)

```
pacman -S lightdm

pacman -S lightdm-gtk-greeter

pacman -S lightdm-gtk-greeter-settings
```

## Activer le service lightdm

```
sudo systemctl enable lightdm
```

## Ajouter une entrée pour DWM dans le DM

Ouvrir ce fichier:

```
mkdir /usr/share/xsessions

vim /usr/share/xsessions/dwm.desktop
```

Ajouter ceci:

```
[Desktop Entry]
Encoding=UTF-8
Name=Dwm
Comment=the dynamic window manager
Exec=dwm
Icon=dwm
Type=XSession
```

## Commandes Basiques

- Moving between windows: `[Alt]+[j] or [Alt]+[k]`

- To move a terminal to another tag: `[Shift]+[Alt]+[<TAG_NUMBER>]`

- To focus on another tag: `[Alt]+[tag number]`

- To change the amount of windows in the master area: `[Alt]+[d] (Decrease) or [Alt]+[i] (Increase)`

- To toggle a window between the master and stack area: `[Alt]+[Return]`

- To kill a window: `[Shift]+[Alt]+[c]`

- Click another tag with the right mouse button to bring those windows into your current focus.

## Dispositions

( **Note** ) Par défaut, dwm est en mode mosaïque.
- Tiled: `[Alt]+[t]`

- Floating: `[Alt]+[f]`

- Monocle: `[Alt]+[m]`

## Flottante

Pour redimensionner la fenêtre flottante : `[Alt]+[right mouse button]`

Pour le déplacer : `[Alt]+[left mouse button]`

**Flottant dans la disposition en mosaïque**

- Bascule le mode flottant sur la fenêtre active : `[Alt]+[Shift]+[space]`

- Redimensionner la fenêtre:  `[Alt]+[right mouse button]`

- Le faire basculer en étant flottant `[Alt]+[middle mouse button]`


## Abandonner

Pour quitter dwm: `[Shift]+[Alt]+[q]`